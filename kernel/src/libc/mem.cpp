//
// Created by main on 3/7/19.
//

#include "libc/mem.hpp"

void* std::memcpy(void *dstptr, const void *srcptr, size_t size) {
    auto* dst = (unsigned char*) dstptr;
    auto* src = (const unsigned char*) srcptr;

    for (size_t i = 0; i < size; i++) {
        dst[i] = src[i];
    }

    return dstptr;
}

void *std::memmov(void *dstptr, void *srcptr, size_t size) {
    memcpy(dstptr, srcptr, size);
    memset(srcptr, size, 0);

    return dstptr;
}
