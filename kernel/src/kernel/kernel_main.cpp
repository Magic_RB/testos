//
// Created by main on 3/7/19.
//

#include "libc/string.hpp"
#include "kernel/vga.hpp"

extern "C" void kernel_main() {
    kernel::vga::initialize();

    kernel::vga::write(std::string64_t("0x") + std::string64_t(100000, 16) + std::string64_t(" that should be in hexadecimal!"));
}