//
// Created by main on 3/8/19.
//

#include "libc/mem.hpp"
#include "kernel/vga.hpp"


kernel::vga::cursor_t kernel::vga::cursor = {0, 0};
kernel::vga::entry_t kernel::vga::color = {};

void kernel::vga::initialize() {
    color.bg = color_t::VGA_COLOR_BLACK;
    color.fg = color_t::VGA_COLOR_WHITE;

    color.ch = ' ';

    for (size_t i = 0; i < width * height; i++) {
        buffer[i] = color.to_raw();
    }

    color.ch = '\0';
}
