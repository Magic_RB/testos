//
// Created by main on 3/7/19.
//

#ifndef BAREBONESV1_STRING_HPP
#define BAREBONESV1_STRING_HPP

#include <stddef.h>

#include "mem.hpp"

namespace std {
    template<size_t size>
    class string_t {
    public:
        string_t() {
            memset(data_m, '\0', size);
        }

        explicit string_t(const char *ptr) {
            memset(data_m, '\0', size);

            char* tmp = (char*)ptr;
            size_t i = 0;

            while(*tmp != '\0' && i < size - 1) {
                data_m[i] = *tmp;
                tmp++;
                i++;
            }
        }

        template <class T>
        string_t(T num, unsigned int base) {
            const char digits[] = "0123456789ABCDEF";
            const unsigned int max_base = 16;
            bool negative = false;

            if (base > max_base)
                base = max_base;

            if (num < 0) {
                negative = true;
                num *= -1;
            }

            char* ptr = &data_m[size - 1];

            do
            {
                ptr--;

                *ptr = digits[num % base];
                num /= base;
            } while (num > 0);

            if (!negative)
                memcpy(data_m, ptr, data_m + size - ptr - 1);
            else {
                memcpy(data_m + 1, ptr, data_m + size - ptr - 1);
                data_m[0] = '-';
            }
        }

        ~string_t() = default;

        string_t<size> copy() const {
            string_t<size> str;

            memcpy(str.data_m, this->data_m, size);

            return str;
        }

        template <size_t other_size>
        string_t<other_size> copy() const {
            string_t<other_size> str;

            memcpy(str.data_m, this->data_m, other_size);

            return str;
        }

        size_t capacity() const {
            return size;
        }

        size_t len() const {
            size_t i = 0;

            while(data_m[++i] != '\0');

            return i;
        }

        char* c_str() {
            return &data_m[0];
        }

        template <size_t other_size>
        string_t<size> operator+(const string_t<other_size>& other) {
            string_t<size> str = this->copy();

            if (size - str.len() - 1 >= other.len()) {
                memcpy(&str.data_m[str.len()], other.data_m, other.len());
            } else {
                memcpy(&str.data_m[str.len()], other.data_m, size - str.len() - 1);
            }

            return str;
        }

    private:
        char data_m[size] = {};
    };

    typedef string_t<64> string64_t;
}

#endif //BAREBONESV1_STRING_HPP
