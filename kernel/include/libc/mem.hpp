//
// Created by main on 3/7/19.
//

#ifndef BAREBONESV1_MEM_HPP
#define BAREBONESV1_MEM_HPP

#include <stddef.h>

namespace std {
    void *memcpy(void *dstptr, const void *srcptr, size_t size);

    template<class T>
    void *memset(void *dstptr, T value, size_t size) {
        auto *dst = (T *) dstptr;

        for (size_t i = 0; i < size; i++) {
            dst[i] = value;
        }

        return dstptr;
    }

    void *memmov(void *dstptr, void *srcptr, size_t size);
}

#endif //BAREBONESV1_MEM_HPP
