//
// Created by main on 3/9/19.
//

#ifndef BAREBONESV1_ARRAY_HPP
#define BAREBONESV1_ARRAY_HPP

#include <stddef.h>

namespace std {

    template <class T, size_t size>
    class array {
    public:

    private:
        T data_m[size];
    };
}

#endif //BAREBONESV1_ARRAY_HPP
