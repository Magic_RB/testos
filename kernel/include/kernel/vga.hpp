//
// Created by main on 3/8/19.
//

#ifndef BAREBONESV1_VGA_HPP
#define BAREBONESV1_VGA_HPP

#include <stdint.h>
#include <stddef.h>

#include "libc/string.hpp"

namespace kernel {
    namespace vga {
        enum color_t {
            VGA_COLOR_BLACK = 0,
            VGA_COLOR_BLUE = 1,
            VGA_COLOR_GREEN = 2,
            VGA_COLOR_CYAN = 3,
            VGA_COLOR_RED = 4,
            VGA_COLOR_MAGENTA = 5,
            VGA_COLOR_BROWN = 6,
            VGA_COLOR_LIGHT_GREY = 7,
            VGA_COLOR_DARK_GREY = 8,
            VGA_COLOR_LIGHT_BLUE = 9,
            VGA_COLOR_LIGHT_GREEN = 10,
            VGA_COLOR_LIGHT_CYAN = 11,
            VGA_COLOR_LIGHT_RED = 12,
            VGA_COLOR_LIGHT_MAGENTA = 13,
            VGA_COLOR_LIGHT_BROWN = 14,
            VGA_COLOR_WHITE = 15,
        };

        struct entry_t {
            uint8_t bg : 4;
            uint8_t fg : 4;
            char ch;

            uint16_t to_raw() {
                uint16_t uint16 = (uint16_t)ch;
                uint16 |= (uint16_t)fg << 8;
                uint16 |= (uint16_t)bg << 12;

                return uint16;
            }
        };

        static uint16_t* buffer = (uint16_t*) 0xB8000;
        static size_t width = 80;
        static size_t height = 25;

        struct cursor_t {
            size_t x;
            size_t y;
        };

        extern cursor_t cursor;
        extern entry_t color;

        void initialize();
        void inline putchat(char ch, size_t x, size_t y) {
            if (x >= width) x = width - 1;
            if (y >= height) y = height - 1;

            color.ch = ch;

            buffer[y * width + x] = color.to_raw();

            color.ch = '\0';
        }

        void inline putch(char ch) {
            putchat(ch, cursor.x, cursor.y);

            if (++cursor.x == width) {
                cursor.x = 0;
                if (++cursor.y == height) {
                    std::memcpy(buffer, buffer + width, (width * height - width) * 2);
                    std::memset(buffer + width * height - width, color, width);

                    cursor.y--;
                }
            }
        }

        void inline write(const char* data, size_t size) {
            for (size_t i = 0; i < size; i++)
                putch(data[i]);
        }

        template <size_t size>
        void inline write(std::string_t<size> str) {
            write(str.c_str(), str.len());
        }
    }
}

#endif //BAREBONESV1_VGA_HPP
