#!/bin/bash

cd ..
sudo qemu-nbd --connect=/dev/nbd0 qemu/test.img
sudo mkdir /tmp/mount -p
sudo mount /dev/nbd0p1 /tmp/mount
sudo cp runtime/i686-elf/kernel /tmp/mount/kernel
sudo umount /tmp/mount
sudo qemu-nbd --disconnect /dev/nbd0
cd qemu
qemu-system-i386 -drive file=test.img
